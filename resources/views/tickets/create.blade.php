@extends('master')
@section('title', 'Create a ticket')

@section('content')
    {{-- <div class="container col-md-8 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header ">
                <h5 class="float-left">Create a ticket</h5>
                <div class="clearfix"></div>
            </div>
            <div class="card-body mt-2">
                <form>
                    <fieldset>
                        <div class="form-group">
                            <label for="title" class="col-lg-12 control-label">Title</label>
                            <div class="col-lg-12">
                                <input type="text" class="form-control" id="title" placeholder="Title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="content" class="col-lg-12 control-label">Content</label>
                            <div class="col-lg-12">
                                <textarea class="form-control" rows="3" id="content"></textarea>
                                <span class="help-block">Feel free to ask us any question.</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div> --}}

    {!! Form::model($ticket, ['action' => 'TicketsController@store']) !!}
    {{ Form::token() }}
    @foreach ($errors->all() as $error)
    <p class="alert alert-danger">{{ $error }}</p>
@endforeach
    <div class="form-group">
      {!! Form::label('title', 'Title') !!}
      {!! Form::text('title', '', ['class' => 'form-control', 'placeholder'=> 'Title']) !!}
    </div>

    <div class="form-group">
      {!! Form::label('content', 'Content') !!}
      {!! Form::textarea('content', '', ['class' => 'form-control', 'placeholder'=> 'Content']) !!}
    </div>

    {!! Form::button('Add the Ticket!',['type' => 'submit', 'class' => 'btn btn-success']) !!}
    {!! Form::button('Cancel', ['type' => 'reset', 'class' => 'btn btn-danger']) !!}

    <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
            <button class="btn btn-default">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

    {!! Form::close() !!}
@endsection
