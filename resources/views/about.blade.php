@extends('master')
@section('title', 'About')

@section('content')
    <div class="container">
        <div class="content">
            <div class="title">About Page</div>
            <div class="quote">{{ \Illuminate\Foundation\Inspiring::quote() }}</div>
        </div>

        <div style="font-size: 0.5rem;">
            <i class="fas fa-camera fa-xs"></i>
            <i class="fas fa-camera fa-sm"></i>
            <i class="fas fa-camera fa-lg"></i>
            <i class="fas fa-camera fa-2x"></i>
            <i class="fas fa-camera fa-3x"></i>
            <i class="fas fa-camera fa-5x"></i>
            <i class="fas fa-camera fa-7x"></i>
            <i class="fas fa-camera fa-10x"></i>
        </div>

        <div class="fa-3x">
            <i class="fas fa-spinner fa-spin"></i>
            <i class="fas fa-spinner fa-pulse"></i>
          </div>
    </div>
@endsection
