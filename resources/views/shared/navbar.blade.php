<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
        <h5 class="my-0 mr-md-auto font-weight-normal">Learning Laravel</h5>
        <nav class="my-2 my-md-0 mr-md-3">
          <a class="p-2 text-dark" href="/">Home</a>
          <a class="p-2 text-dark" href="/about">About</a>
          <a class="p-2 text-dark" href="/contact">Contact</a>
        </nav>
        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Member <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                <li><a href="/users/register">Register</a></li>
                <li><a href="/users/login">Login</a></li>
                </ul>
                </li>
      </div>
