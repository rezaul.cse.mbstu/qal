<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{!! asset('css/app.css') !!}" >

    <!-- Material Design for Bootstrap fonts and icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">

    <!-- Material Design for Bootstrap CSS -->
    <link rel="stylesheet" href="{!! asset('css/bootstrap-material-design.min.css') !!}" >

    <!-- Fontawesome CSS -->
    <link href="{!! asset('css/all.css') !!}" rel="stylesheet"> <!--load all styles -->

    <title> @yield('title') </title>
    <link rel="shortcut icon" type="image/icon" href="{!! asset('icon/favicon-cloud.ico') !!}"/>
  </head>
  <body>
        @include('shared.navbar')

        @yield('content')



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{!! asset('js/jquery-3.4.1.min.js') !!}"></script>
    <script src="{!! asset('js/popper.min.js') !!}"></script>
    {{-- <script src="{!! asset('js/bootstrap.min.js') !!}"></script> --}}
    <script src="{!! asset('js/bootstrap-material-design.js') !!}"></script>
    <script>
        $(document).ready(function() {
            $('body').bootstrapMaterialDesign();
        });
    </script>

  </body>
</html>
