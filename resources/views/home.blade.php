@extends('master')
@section('title', 'Home')

@section('content')
    {{-- <div class="container">
        <div class="content">
            <div class="title">Home Page</div>
            <div class="quote">{{ \Illuminate\Foundation\Inspiring::quote() }}</div>
        </div>

        <div style="font-size: 0.5rem;">
            <i class="fas fa-camera fa-xs"></i>
            <i class="fas fa-camera fa-sm"></i>
            <i class="fas fa-camera fa-lg"></i>
            <i class="fas fa-camera fa-2x"></i>
            <i class="fas fa-camera fa-3x"></i>
            <i class="fas fa-camera fa-5x"></i>
            <i class="fas fa-camera fa-7x"></i>
            <i class="fas fa-camera fa-10x"></i>
        </div>

        <div class="fa-3x">
            <i class="fas fa-spinner fa-spin"></i>
            <i class="fas fa-spinner fa-pulse"></i>
          </div>
    </div> --}}

    <div class="container">
            <div class="row banner">
            <div class="col-md-12">
            <h1 class="text-center margin-top-100 editContent">
            Learning Laravel 5, 6th Edition
            </h1>
            <h3 class="text-center margin-top-100 editContent">Building Applications with Bootstrap 4</h3>
            <div class="text-center">
            <img src="https://learninglaravel.net/img/LearningLaravel5_cover6.png" width="302" height="391" alt="">
            </div>
            </div>
            </div>
            </div
@endsection
