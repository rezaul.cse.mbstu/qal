<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //belongs to user
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //get ticket title
    public function getTitle()
    {
        return $this->title;
    }
}
