<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    /** return home page
     *
     */
    public function home()
    {
        return view('home');
    }

    /** return about page
     *
     */
    public function about()
    {
        return view('about');
    }

    /** return contact page
     *
     */
    public function contact()
    {
        return view('contact');
    }
}
